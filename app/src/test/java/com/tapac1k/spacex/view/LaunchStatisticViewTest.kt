package com.tapac1k.spacex.view

import com.tapac1k.spacex.entities.Status
import com.tapac1k.spacex.utils.LaunchGenerator
import com.tapac1k.spacex.view.statisticview.StatisticHelper
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class LaunchStatisticViewTest {
    lateinit var statisticHelper: StatisticHelper
    val calendar =  Calendar.getInstance()


    @Before
    fun prepare() {
        statisticHelper = StatisticHelper()

        calendar.set(2020,5,30)
        val listDate1 = LaunchGenerator.createLaunches(calendar.time, 5,0,0,1)
        calendar.set(2019,4,30)
        val listDate2 = LaunchGenerator.createLaunches(calendar.time, 0,5,5,1)
        calendar.set(2018,11,30)
        val listDate3 = LaunchGenerator.createLaunches(calendar.time, 1,0,0,0)
        calendar.set(2018,11,28)
        val listDate4 = LaunchGenerator.createLaunches(calendar.time, 3,1,0,0)
        val list = listDate1 + listDate2 + listDate3 + listDate4

        statisticHelper.process(list)
    }


    @Test
    fun launchesPerDateTest() {
        calendar.set(2020,5,1)
        assertEquals(1, statisticHelper.getCountByMonth(calendar.time, Status.UNKNOWN))
        calendar.set(2019,4,1)
        assertEquals(5, statisticHelper.getCountByMonth(calendar.time, Status.SUCCESS))
        calendar.set(2018,11,1)
        assertEquals(4, statisticHelper.getCountByMonth(calendar.time, Status.UPCOMING))
    }

    @Test
    fun monthFrameTest() {
        val actualStartMonth = statisticHelper.startMonth
        val actualMonthCount = statisticHelper.monthCount

        assertEquals(201811, actualStartMonth)
        assertEquals(19, actualMonthCount)
    }

    @Test
    fun maxTotalTest() {
        val actualMaxTotal = statisticHelper.maxTotal

        assertEquals(11, actualMaxTotal)
    }


}