package com.tapac1k.spacex.utils

import com.tapac1k.spacex.domain.entities.Launch
import java.util.*

object LaunchGenerator {
    fun createLaunches(date: Date, upcoming: Int, success: Int, failed: Int, unknown: Int): List<Launch> {
        val generated = arrayListOf<Launch>()
        var counter = 1
        for(i in 1..upcoming) {
            val launch = Launch(
                flightNumber = counter,
                missionName = "Bla",
                launchDate = date,
                rocketName = "rocket",
                success = null,
                upcoming = true
            )

            generated.add(launch)
            counter++
        }

        for(i in 1..success) {
            val launch = Launch(
                flightNumber = counter,
                missionName = "Bla",
                launchDate = date,
                rocketName = "rocket",
                success = true,
                upcoming = false
            )

            generated.add(launch)
            counter++
        }

        for(i in 1..failed) {
            val launch = Launch(
                flightNumber = counter,
                missionName = "Bla",
                launchDate = date,
                rocketName = "rocket",
                success = false,
                upcoming = false
            )

            generated.add(launch)
            counter++
        }

        for(i in 1..unknown) {
            val launch = Launch(
                flightNumber = counter,
                missionName = "Bla",
                launchDate = date,
                rocketName = "rocket",
                success = null,
                upcoming = false
            )

            generated.add(launch)
            counter++
        }

        return generated
    }


}