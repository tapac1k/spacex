package com.tapac1k.spacex.entities

import com.tapac1k.spacex.R

enum class Status(val textId: Int, val colorId: Int) {
    UNKNOWN(R.string.unknown, R.color.common_grey_text),
    SUCCESS(R.string.success, R.color.green),
    UPCOMING(R.string.upcoming, R.color.yellow),
    FAILED(R.string.failed, R.color.red)
}