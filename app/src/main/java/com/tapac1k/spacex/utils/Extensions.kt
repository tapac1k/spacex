package com.tapac1k.spacex.utils

import android.content.res.Resources
import com.tapac1k.spacex.domain.entities.Launch
import com.tapac1k.spacex.entities.Status
import kotlin.math.roundToInt

val Launch.status: Status
    get() {
        return when {
            upcoming -> Status.UPCOMING
            success == null -> Status.UNKNOWN
            success!! -> Status.SUCCESS
            else -> Status.FAILED
        }
    }

fun Int.toPx(): Int {
    return (this * Resources.getSystem().displayMetrics.density).roundToInt()
}