package com.tapac1k.spacex.utils

import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.tapac1k.spacex.R
import com.tapac1k.spacex.domain.entities.Launch
import com.tapac1k.spacex.entities.Status
import com.tapac1k.spacex.view.statisticview.LaunchStatisticView
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


private val dateFormat: DateFormat = SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT)


private val dateTimeFormat: DateFormat = SimpleDateFormat.getDateTimeInstance(SimpleDateFormat.SHORT, SimpleDateFormat.SHORT)

@BindingAdapter("android:imageUrl")
fun setImageUrl(imageView: ImageView, url: String?) {
    url?: imageView.setImageDrawable(null)
    Glide.with(imageView)
        .load(url)
        .placeholder(R.drawable.ic_placeholder)
        .error(R.drawable.ic_none)
        .into(imageView)
}

@BindingAdapter("android:date")
fun setDate(textView: TextView, date: Date?) {
    date?: return
    textView.text = dateFormat.format(date)
}

@BindingAdapter("android:datetime")
fun setDateTime(textView: TextView, date: Date?) {
    date?: return
    textView.text = dateTimeFormat.format(date)
}

@BindingAdapter("android:status")
fun setLaunchStatus(textView: TextView, status: Status?) {
    status?.let{
        textView.setText(it.textId)
        textView.setTextColor(ContextCompat.getColor(textView.context, it.colorId))
    }?: run{
        textView.text = ""
    }
}

@BindingAdapter("android:data")
fun setLaunchData(view: LaunchStatisticView, data: List<Launch>?) {
    view.setLaunchList(data)
}


