package com.tapac1k.spacex.feature.statistic

import androidx.navigation.NavArgs
import com.tapac1k.spacex.common.SpaceXViewModel
import com.tapac1k.spacex.common.delegate
import com.tapac1k.spacex.domain.interactor.launch.GetAllLaunchesUseCase

class StatisticViewModel(
    getAllLaunchesUseCase: GetAllLaunchesUseCase
) : SpaceXViewModel<StatisticFragment.Command, NavArgs>() {

    val launchData by getAllLaunchesUseCase.delegate()

}