package com.tapac1k.spacex.feature.launchdetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tapac1k.spacex.databinding.LaunchDetailsImageItemBinding


class ImageAdapter : RecyclerView.Adapter<ImageAdapter.ImageViewHolder>() {
    private var imageList = arrayListOf<String>()

    var onClickListener: ((String) -> Unit)? = null
    var onDownloadClickListener: ((String) -> Unit)? = null

    private val itemClickListener = { it: String -> onClickListener?.invoke(it) }
    private val downloadItemClickListener = { it: String -> onDownloadClickListener?.invoke(it) }

    class ImageViewHolder(val binding: LaunchDetailsImageItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LaunchDetailsImageItemBinding.inflate(
            inflater,
            parent,
            false
        )
        return ImageViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val url = imageList[position]
        with(holder.binding) {
            if (item == null) {
                item = ImageItemViewModel(url, itemClickListener, downloadItemClickListener)
            } else {
                item!!.bind(url)
            }
        }
    }

    fun submitList(list: List<String>) {
        imageList.clear()
        imageList.addAll(list)
        notifyDataSetChanged()
    }

    override fun getItemCount() = imageList.size
}