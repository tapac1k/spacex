package com.tapac1k.spacex.feature.launchlist

import androidx.databinding.ObservableField
import com.tapac1k.spacex.domain.entities.Launch
import com.tapac1k.spacex.entities.Status
import com.tapac1k.spacex.utils.status
import java.util.*

class LaunchItemViewModel(
    private var launch: Launch,
    private val onClickListener: ((Launch) -> Unit?)?
) {

    val title = ObservableField<String>()
    val badgeUrl = ObservableField<String>()
    val rocket = ObservableField<String>()
    val statusField = ObservableField<Status>()
    val date = ObservableField<Date>()

    init {
        bind(launch)
    }

    fun bind(launch: Launch) {
        this.launch = launch
        with(launch) {
            title.set("$flightNumber. $missionName")
            badgeUrl.set(badgeLink)
            rocket.set(rocketName)
            date.set(launchDate)
            statusField.set(status)
        }
    }

    fun onClick() {
        onClickListener?.invoke(launch)
    }


}