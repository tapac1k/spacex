package com.tapac1k.spacex.feature.main

import androidx.navigation.NavArgs
import com.tapac1k.spacex.common.SpaceXViewModel
import com.tapac1k.spacex.domain.interactor.launch.SyncLaunchesUseCase

class MainViewModel(
    private val syncLaunchesUseCase: SyncLaunchesUseCase
) : SpaceXViewModel<MainFragment.Command, NavArgs>() {

    override fun initWithArguments(args: NavArgs?) {
        super.initWithArguments(args)
        syncLaunchesUseCase.execute()
    }
}