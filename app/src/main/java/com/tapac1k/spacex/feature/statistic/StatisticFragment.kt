package com.tapac1k.spacex.feature.statistic

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavArgs
import com.tapac1k.spacex.common.SpaceXFragment
import com.tapac1k.spacex.databinding.StatisticsFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class StatisticFragment : SpaceXFragment<StatisticFragment.Command, NavArgs>() {
    lateinit var binding: StatisticsFragmentBinding

    private val viewModel by viewModel<StatisticViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = StatisticsFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = viewModel
        initViewModel(viewModel, null)
    }

    enum class Command
}