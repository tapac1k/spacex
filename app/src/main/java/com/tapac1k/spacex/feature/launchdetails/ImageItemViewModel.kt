package com.tapac1k.spacex.feature.launchdetails

import androidx.databinding.ObservableField

class ImageItemViewModel(
    private var url: String,
    private val onClickListener: ((String) -> Unit?)?,
    private val onDownloadClickListener: ((String) -> Unit?)?
) {

    val imageLink = ObservableField(url)

    fun bind(url: String) {
        this.url = url
        imageLink.set(url)
    }

    fun onClick() {
        onClickListener?.invoke(url)
    }

    fun onDownload() {
        onDownloadClickListener?.invoke(url)
    }
}