package com.tapac1k.spacex.feature.launchdetails

import android.content.res.Resources
import androidx.core.os.bundleOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.tapac1k.spacex.R
import com.tapac1k.spacex.common.SpaceXViewModel
import com.tapac1k.spacex.common.delegate
import com.tapac1k.spacex.domain.entities.Launch
import com.tapac1k.spacex.domain.interactor.RequestImageDownloadUseCase
import com.tapac1k.spacex.domain.interactor.launch.GetLaunchByIdUseCase
import com.tapac1k.spacex.domain.interactor.launch.GetNextLaunchIdAfterIdUseCase
import com.tapac1k.spacex.domain.interactor.launch.GetPreviousLaunchIdAfterIdUseCase
import com.tapac1k.spacex.entities.Status
import com.tapac1k.spacex.utils.status

class LaunchDetailsViewModel(
    private val resources: Resources,
    getLaunchByIdUseCase: GetLaunchByIdUseCase,
    getNextLaunchIdAfterIdUseCase: GetNextLaunchIdAfterIdUseCase,
    getPreviousLaunchIdAfterIdUseCase: GetPreviousLaunchIdAfterIdUseCase,
    private val requestImageDownloadUseCase: RequestImageDownloadUseCase
): SpaceXViewModel<LaunchDetailsFragment.Command, LaunchDetailsFragmentArgs>() {

    private val currentPage = MutableLiveData<Int>()

    val nextPage by getNextLaunchIdAfterIdUseCase.delegate(currentPage)
    val previousPage by getPreviousLaunchIdAfterIdUseCase.delegate(currentPage)
    val currentLaunch by getLaunchByIdUseCase.delegate(currentPage)
    val currentLaunchStatus: LiveData<Status> = Transformations.map(currentLaunch) {
        it?.status?: Status.UNKNOWN
    }

    override fun initWithArguments(args: LaunchDetailsFragmentArgs?) {
        requireNotNull(args)
        loadDetails(args.id)
    }

    private fun loadDetails(id: Int) {
        currentPage.postValue(id)
    }

    fun onDownloadClick(url: String) {
        val filename = "SpaceX${System.currentTimeMillis()}.jpg"
        val params = RequestImageDownloadUseCase.Params(
            url,
            filename,
            resources.getString(R.string.image_download)
        )
        requestImageDownloadUseCase.execute(params, onComplete = {
            notifyEvent(
                LaunchDetailsFragment.Command.SHOW_TOAST,
                bundleOf("textId" to R.string.download_started ) )
        })
    }

    fun onImageClick(url: String) {
        notifyEvent(
            LaunchDetailsFragment.Command.NAVIGATE_URL,
            bundleOf("url" to url)
        )
    }

    fun onYoutube() {
        notifyEvent(
            LaunchDetailsFragment.Command.NAVIGATE_URL,
            bundleOf("url" to currentLaunch.value!!.youtubeLink)
        )
    }

    fun onPress() {
        notifyEvent(
            LaunchDetailsFragment.Command.NAVIGATE_URL,
            bundleOf("url" to currentLaunch.value!!.pressKitLink)
        )
    }

    fun onWikipedia() {
        notifyEvent(
            LaunchDetailsFragment.Command.NAVIGATE_URL,
            bundleOf("url" to currentLaunch.value!!.wikipediaLink)
        )
    }

    fun onReddit() {
        notifyEvent(
            LaunchDetailsFragment.Command.NAVIGATE_URL,
            bundleOf("url" to currentLaunch.value!!.moreImagesLink)
        )
    }

    fun onNextPage() {
        nextPage.value?.let { loadDetails(it) }
    }

    fun onPreviousPage() {
        previousPage.value?.let { loadDetails(it) }
    }
}