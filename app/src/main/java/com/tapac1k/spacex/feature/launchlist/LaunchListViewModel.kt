package com.tapac1k.spacex.feature.launchlist

import androidx.core.os.bundleOf
import androidx.navigation.NavArgs
import com.tapac1k.spacex.common.SpaceXViewModel
import com.tapac1k.spacex.common.delegate
import com.tapac1k.spacex.domain.entities.Launch
import com.tapac1k.spacex.domain.interactor.launch.ListenLaunchesUseCase

class LaunchListViewModel(
    listenLaunchesUseCase: ListenLaunchesUseCase
) : SpaceXViewModel<LaunchListFragment.Command, NavArgs>() {

    val launchData by listenLaunchesUseCase.delegate()

    fun onLaunchClick(launch: Launch) {
        notifyEvent(
            LaunchListFragment.Command.NAVIGATE_LAUNCH_DETAILS,
            bundleOf("id" to launch.flightNumber)
        )
    }

}