package com.tapac1k.spacex.feature.launchlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.NavArgs
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.tapac1k.spacex.R
import com.tapac1k.spacex.common.SpaceXFragment
import com.tapac1k.spacex.databinding.LaunchListFragmentBinding
import com.tapac1k.spacex.feature.main.MainFragmentDirections
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class LaunchListFragment : SpaceXFragment<LaunchListFragment.Command, NavArgs>() {
    private lateinit var binding: LaunchListFragmentBinding
    private val viewModel by viewModel<LaunchListViewModel>()

    private lateinit var navController: NavController

    private val launchAdapter by inject<LaunchAdapter>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LaunchListFragmentBinding.inflate(inflater, container, false)
        with(binding.recyclerLaunch) {
            layoutManager = LinearLayoutManager(inflater.context)
            adapter = launchAdapter
            launchAdapter.onClickListener = {
                viewModel.onLaunchClick(it)
            }
        }
        navController = Navigation.findNavController(activity!!, R.id.nav_host_fragment)
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = viewModel
        initViewModel(viewModel, null)
        viewModel.launchData.observe(viewLifecycleOwner, Observer {
            launchAdapter.submitList(it)
        })
    }

    override fun onEvent(event: Command, extras: Bundle?) {
        when (event) {
            Command.NAVIGATE_LAUNCH_DETAILS -> {
                val action =
                    MainFragmentDirections.actionMainFragmentToLaunchDetailsFragment(
                        extras!!.getInt(
                            "id"
                        )
                    )
                navController.navigate(action)
            }
        }
    }

    enum class Command {
        NAVIGATE_LAUNCH_DETAILS
    }
}