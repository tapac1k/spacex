package com.tapac1k.spacex.feature.launchdetails

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.tapac1k.spacex.R
import com.tapac1k.spacex.common.SpaceXFragment
import com.tapac1k.spacex.databinding.LaunchDetailsFragmentBinding
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class LaunchDetailsFragment :
    SpaceXFragment<LaunchDetailsFragment.Command, LaunchDetailsFragmentArgs>() {

    lateinit var binding: LaunchDetailsFragmentBinding

    private lateinit var navController: NavController
    private val viewModel by viewModel<LaunchDetailsViewModel>()

    private val imageAdapter by inject<ImageAdapter>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LaunchDetailsFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        navController = Navigation.findNavController(activity!!, R.id.nav_host_fragment)
        with(binding.recyclerImage) {
            layoutManager = GridLayoutManager(requireContext(), 3)
            adapter = imageAdapter
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.viewModel = viewModel
        initViewModel(viewModel, LaunchDetailsFragmentArgs.fromBundle(requireArguments()))
        viewModel.currentLaunch.observe(viewLifecycleOwner, Observer {
            it ?: return@Observer
            (activity as AppCompatActivity?)?.supportActionBar?.title = it.missionName
            imageAdapter.submitList(it.images)
        })
        imageAdapter.onClickListener = {
            viewModel.onImageClick(it)
        }
        imageAdapter.onDownloadClickListener = {
            viewModel.onDownloadClick(it)
        }
    }

    override fun onEvent(event: Command, extras: Bundle?) {
        when (event) {
            Command.NAVIGATE_URL -> {
                CustomTabsIntent.Builder()
                    .build()
                    .launchUrl(requireContext(), Uri.parse(extras!!.getString("url")))

            }
            Command.SHOW_TOAST -> {
                Toast.makeText(
                    requireContext(),
                    extras!!.getInt("textId"),
                    Toast.LENGTH_SHORT
                ).show()
            }
            Command.CHECK_PERMISSION -> {

            }
        }
    }

    enum class Command {
        NAVIGATE_URL,
        SHOW_TOAST,
        CHECK_PERMISSION
    }

}