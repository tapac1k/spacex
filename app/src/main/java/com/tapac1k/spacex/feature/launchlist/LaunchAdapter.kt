package com.tapac1k.spacex.feature.launchlist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tapac1k.spacex.databinding.LaunchListLaunchItemBinding
import com.tapac1k.spacex.domain.entities.Launch

private val launchDiffUtil =  object:DiffUtil.ItemCallback<Launch>(){
    override fun areItemsTheSame(oldItem: Launch, newItem: Launch): Boolean {
        return oldItem.flightNumber == newItem.flightNumber
    }

    override fun areContentsTheSame(oldItem: Launch, newItem: Launch): Boolean {
        return oldItem == newItem
    }
}

class LaunchAdapter: ListAdapter<Launch, LaunchAdapter.LaunchViewHolder>(launchDiffUtil) {

    var onClickListener: ((Launch) -> Unit)? = null

    private val itemClickListener = { it: Launch -> onClickListener?.invoke(it) }

    class LaunchViewHolder(val binding: LaunchListLaunchItemBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LaunchViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LaunchListLaunchItemBinding.inflate(
            inflater,
            parent,
            false
        )
        return LaunchViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LaunchViewHolder, position: Int) {
        val launch = getItem(position)?: return
        with(holder.binding) {
            if(item == null) {
                item = LaunchItemViewModel(launch, itemClickListener )
            } else {
                item!!.bind(launch)
            }
        }
    }
}