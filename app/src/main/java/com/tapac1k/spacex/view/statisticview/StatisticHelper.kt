package com.tapac1k.spacex.view.statisticview

import com.tapac1k.spacex.domain.entities.Launch
import com.tapac1k.spacex.entities.Status
import com.tapac1k.spacex.utils.status
import java.util.*

/**
 * Class to prepare launch data to be displayed on chart
 *
 * To process data call [StatisticHelper.process]
 */
class StatisticHelper {
    private val calendar = Calendar.getInstance()
    private val launchMonthMap = hashMapOf<Int, Month>()

    var startMonth = 0
        private set
    var monthCount = 0
        private set
    var maxTotal = 0
        private set

    /**
     * Process launches data from [list]
     *
     * You should call this method first
     */
    fun process(list: List<Launch>?) {
        launchMonthMap.clear()
        startMonth = 0
        monthCount = 0
        maxTotal = 0
        if (list.isNullOrEmpty()) return
        list.forEach {
            val monthId = getMonthIdFromDate(it.launchDate)
            val month = launchMonthMap.getOrPut(monthId, {
                Month(
                    monthId
                )
            })
            month.addLaunch(it)
        }
        val first = launchMonthMap.keys.min()!!
        val last = launchMonthMap.keys.max()!!

        val count = (last / 100 - first / 100) * 12 + last % 100 - first % 100 + 1

        startMonth = first
        monthCount = count
        maxTotal = launchMonthMap.values.maxBy { it.total }!!.total
    }

    /**
     * Get month of first launch in list passed to [process]
     */
    fun getStartDate(): Date {
        calendar.set(startMonth / 100, startMonth % 100, 1)
        return calendar.time
    }

    /**
     * Get count of launches with [status] in month [date]
     *
     *  if [status] is null method returns total count
     */
    fun getCountByMonth(date: Date, status: Status? = null): Int {
        val monthId = getMonthIdFromDate(date)
        val month = launchMonthMap[monthId]
        return when (status) {
            Status.UNKNOWN -> month?.unknown
            Status.SUCCESS -> month?.success
            Status.UPCOMING -> month?.upcoming
            Status.FAILED -> month?.failed
            null -> month?.total
        } ?: 0
    }

    private fun getMonthIdFromDate(date: Date): Int {
        calendar.time = date
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)
        return year * 100 + month
    }


    private class Month(
        val monthId: Int,
        var total: Int = 0,
        var upcoming: Int = 0,
        var success: Int = 0,
        var failed: Int = 0,
        var unknown: Int = 0
    ) {

        fun addLaunch(launch: Launch) {
            total++
            when (launch.status) {
                Status.SUCCESS -> success++
                Status.UPCOMING -> upcoming++
                Status.FAILED -> failed++
                else -> unknown++
            }
        }
    }

}