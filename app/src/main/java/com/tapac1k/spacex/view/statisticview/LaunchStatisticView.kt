package com.tapac1k.spacex.view.statisticview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.view.setPadding
import com.tapac1k.spacex.R
import com.tapac1k.spacex.domain.entities.Launch
import com.tapac1k.spacex.entities.Status
import com.tapac1k.spacex.utils.toPx
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt


private val statusOrder = arrayOf(
    Status.FAILED,
    Status.SUCCESS,
    Status.UNKNOWN,
    Status.UPCOMING
)

/**
 * View to display statistic of launches per month
 *
 * To setup view call [setLaunchList]
 *
 * View supports restoring state for [popupText]. To make this work set view id
 */
class LaunchStatisticView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    @SuppressLint("SimpleDateFormat")
    private val simpleDateFormat = SimpleDateFormat("MMM yyyy")

    private val drawCalendar = Calendar.getInstance()
    private val processCalendar = Calendar.getInstance()

    private val monthWidth = 10.toPx()
    private val bottomChartSpace = 20.toPx()
    private val minimumChartHeight = 40.toPx()
    private val startChartSpace = 40.toPx()
    private val minFrameSize = 50.toPx()
    private var pointHeight = 0f
    private val popupWidth = 100.toPx()


    private val chartRect = Rect()
    private var currentMonth: Date? = null

    // paints
    private val successPaint = Paint().apply { color = Color.GREEN }
    private val unknownPaint = Paint().apply { color = Color.GRAY }
    private val failedPaint = Paint().apply { color = Color.RED }
    private val upcomingPaint = Paint().apply { color = Color.YELLOW }
    private val framePaint = Paint().apply { color = Color.BLACK }
    private val innerFramePaint = Paint().apply {
        pathEffect = DashPathEffect(floatArrayOf(10f, 20f), 0f)
        color = Color.DKGRAY
    }
    private val textPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.BLACK
        textSize = 14.toPx().toFloat()
    }

    private val statisticHelper = StatisticHelper()

    init {
        clipToPadding = false
    }

    private val chartView = ChartView(context).also {
        addView(it)
    }

    private val popupText = PopupView(context).also {
        it.isVisible = false
        addView(it, LayoutParams(popupWidth, LayoutParams.MATCH_PARENT))
    }


    fun setLaunchList(list: List<Launch>?) {
        statisticHelper.process(list)
        chartView.requestLayout()
        currentMonth?.let { showPopup(it) }
    }

    override fun onSaveInstanceState(): Parcelable? {
        return bundleOf(
            "superState" to super.onSaveInstanceState(),
            "month" to currentMonth
        )
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        when (state) {
            is Bundle -> {
                super.onRestoreInstanceState(state.getParcelable("superState"))
                currentMonth = state.getSerializable("month") as Date?
            }
            else -> super.onRestoreInstanceState(state)
        }
    }

    private fun hidePopup() {
        currentMonth = null
        popupText.isVisible = false
    }

    private fun getLabelForStatus(status: Status?): String {
        return resources.getString(
            when (status) {
                Status.UNKNOWN -> R.string.unknown_label
                Status.SUCCESS -> R.string.success_label
                Status.UPCOMING -> R.string.upcoming_label
                Status.FAILED -> R.string.failed_label
                null -> R.string.total_label

            }
        )
    }

    private fun getPaint(status: Status): Paint {
        return when (status) {
            Status.UNKNOWN -> unknownPaint
            Status.SUCCESS -> successPaint
            Status.UPCOMING -> upcomingPaint
            Status.FAILED -> failedPaint
        }
    }

    private fun showPopup(date: Date) {
        currentMonth = date
        val stringBuilder = StringBuilder()
        stringBuilder.appendln(simpleDateFormat.format(date))
        stringBuilder.appendln(
            "${getLabelForStatus(null)} ${statisticHelper.getCountByMonth(
                date
            )}"
        )
        for (status in statusOrder) {
            val count = statisticHelper.getCountByMonth(date, status)
            if (count > 0) stringBuilder.appendln("${getLabelForStatus(status)} $count")
        }
        popupText.text = stringBuilder.trim()

        post{
            val x = getCoordByDate(date)
            if (x + popupWidth + paddingEnd > width) {
                popupText.rightToLine = false
                popupText.translationX = x - popupWidth
            } else {
                popupText.rightToLine = true
                popupText.translationX = x
            }
            popupText.isVisible = true
        }

    }

    private fun getCoordByDate(date: Date): Float {
        processCalendar.time = statisticHelper.getStartDate()
        val startYear = processCalendar.get(Calendar.YEAR)
        val startMonth = processCalendar.get(Calendar.MONTH)

        processCalendar.time = date
        val pos = (processCalendar.get(Calendar.YEAR) - startYear) * 12 +
                (processCalendar.get(Calendar.MONTH) - startMonth)
        return startChartSpace + (pos + 0.5f) * monthWidth
    }


    /**
     * Used to display month data after click
     */
    inner class PopupView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
    ) : TextView(context, attrs, defStyleAttr) {

        var rightToLine = true
            set(value) {
                postInvalidate()
                field = value
            }

        init {
            gravity = Gravity.CENTER_VERTICAL
            setTextColor(Color.WHITE)
            setPadding(8.toPx())
        }

        override fun onDraw(canvas: Canvas?) {
            val linePos = if (rightToLine) 1 else width - 1
            canvas?.drawLine(
                linePos.toFloat(),
                0f,
                linePos.toFloat(),
                height.toFloat(),
                innerFramePaint
            )
            val lineHeight = paint.fontMetrics.bottom - paint.fontMetrics.top
            val textHeight = lineHeight * lineCount * lineSpacingMultiplier
            canvas?.drawRect(
                0f,
                (height - textHeight) / 2f,
                width.toFloat(),
                (height + textHeight) / 2f,
                framePaint
            )
            super.onDraw(canvas)
        }
    }

    /**
     * Used to draw chart
     */
    inner class ChartView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
    ) : View(context, attrs, defStyleAttr) {


        override fun getSuggestedMinimumHeight(): Int {
            return (minimumChartHeight + bottomChartSpace)
        }

        override fun getSuggestedMinimumWidth(): Int {
            return (startChartSpace + monthWidth * statisticHelper.monthCount)
        }

        override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
            val desiredWidth = suggestedMinimumWidth + paddingStart + paddingEnd
            val desiredHeight = suggestedMinimumHeight + paddingTop + paddingBottom

            setMeasuredDimension(
                measureDimension(desiredWidth, widthMeasureSpec),
                measureDimension(desiredHeight, heightMeasureSpec)
            )

        }

        @SuppressLint("ClickableViewAccessibility")
        override fun onTouchEvent(event: MotionEvent?): Boolean {
            if (event?.actionMasked == MotionEvent.ACTION_UP) {
                processClick(event.x, event.y)
            }
            return true
        }

        private fun processClick(x: Float, y: Float) {
            if (chartRect.contains(x.roundToInt(), y.roundToInt())) {
                val monthPos = ((x - chartRect.left) / monthWidth).toInt()
                processCalendar.time = statisticHelper.getStartDate()
                processCalendar.add(Calendar.MONTH, monthPos)
                showPopup(processCalendar.time)
            } else {
                hidePopup()
            }
        }

        private fun measureDimension(desiredSize: Int, measureSpec: Int): Int {
            val measureMode = MeasureSpec.getMode(measureSpec)
            val measureSize = MeasureSpec.getSize(measureSpec)
            return when (measureMode) {
                MeasureSpec.EXACTLY -> measureSize
                MeasureSpec.AT_MOST -> min(desiredSize, measureSize)
                MeasureSpec.UNSPECIFIED -> desiredSize
                else -> error("Illegal measure mode value")
            }
        }

        override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
            super.onSizeChanged(w, h, oldw, oldh)
            chartRect.set(
                paddingStart + startChartSpace,
                paddingTop,
                w - paddingEnd,
                h - paddingBottom - bottomChartSpace
            )
            pointHeight = chartRect.height() * 1.0f / statisticHelper.maxTotal
        }

        override fun onDraw(canvas: Canvas?) {
            super.onDraw(canvas)
            drawFrame(canvas)
            drawChart(canvas)
        }

        private fun drawFrame(canvas: Canvas?) {
            canvas?.drawLine(
                chartRect.right.toFloat(),
                chartRect.bottom.toFloat(),
                chartRect.left.toFloat(),
                chartRect.bottom.toFloat(),
                framePaint
            )

            canvas?.drawLine(
                chartRect.left.toFloat(),
                chartRect.top.toFloat(),
                chartRect.left.toFloat(),
                chartRect.bottom.toFloat(),
                framePaint
            )

            // calculate max sector count by view height
            val quantCount = chartRect.height() / minFrameSize
            //calculate point step, step should be int > 0
            val step = max(1, statisticHelper.maxTotal / quantCount)
            val stepPx = step * pointHeight // calculate height of step

            val stepCount = statisticHelper.maxTotal / step
            var pos = chartRect.bottom.toFloat()

            var countLabel = 0

            for (i in 1 until stepCount) {
                pos -= stepPx
                countLabel += step
                canvas?.drawLine(
                    chartRect.left.toFloat(),
                    pos,
                    chartRect.right.toFloat(),
                    pos,
                    innerFramePaint
                )

                canvas?.drawText(
                    "$countLabel",
                    chartRect.left - startChartSpace.toFloat(),
                    pos,
                    textPaint
                )
            }

        }

        private fun drawChart(canvas: Canvas?) {
            if (statisticHelper.maxTotal == 0) return // nothing to draw
            with(statisticHelper) {
                drawCalendar.time = getStartDate()    //setup calendar with first month in data
                var rectLeft = chartRect.left.toFloat()
                for (i in 1..monthCount) {
                    var curBottom = chartRect.bottom.toFloat()
                    //draw rect for each status
                    for (status in statusOrder) {
                        val count = getCountByMonth(drawCalendar.time, status)
                        val top = curBottom - (pointHeight * count)
                        canvas?.drawRect(
                            rectLeft,
                            top,
                            rectLeft + monthWidth,
                            curBottom,
                            getPaint(status)
                        )
                        curBottom = top
                    }
                    drawCalendar.add(Calendar.MONTH, 1)  // go to next month
                    rectLeft += monthWidth

                }
            }
        }

    }

}

