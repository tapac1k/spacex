package com.tapac1k.spacex

import android.app.Application
import com.tapac1k.spacex.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class SpaceXApp: Application() {

    override fun onCreate() {
        super.onCreate()
        launchKoin()
    }

    private fun launchKoin() {

        val modules = listOf(
            coreModule,
            mainModule,
            launchListModule,
            launchDetailsModule,
            statisticsModule
        )

        startKoin {
            if (BuildConfig.DEBUG) {
                androidLogger(Level.DEBUG)
            }
            androidContext(this@SpaceXApp)
            modules(modules)
        }

    }

}