package com.tapac1k.spacex.common

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavArgs

open class SpaceXViewModel<Event : Enum<Event>, Args : NavArgs>: ViewModel() {
    private val mutEvent = MutableLiveData<Consumable<Event>>()

    private var initialized = false

    internal val events: LiveData<Consumable<Event>>
        get() = mutEvent

    protected fun notifyEvent(event: Event, extras: Bundle? = null) {
        mutEvent.postValue(Consumable(event, extras))
    }


    fun initialize(args: Args?) {
        if(initialized) return
        initialized = true
        initWithArguments(args)
    }

    protected open fun initWithArguments(args: Args?) {

    }
}