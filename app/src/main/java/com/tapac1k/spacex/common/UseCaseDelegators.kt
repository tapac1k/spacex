package com.tapac1k.spacex.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations.switchMap
import com.tapac1k.spacex.domain.interactor.common.FlowableUseCase
import com.tapac1k.spacex.domain.interactor.common.ParamFlowableUseCase
import com.tapac1k.spacex.domain.interactor.common.ParamSingleUseCase
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

/**
 * Use this delegators to get data from usecase without boilerplate
 *
 * Also you can observe livedata with params to execute usecase with params
 */

fun <T> FlowableUseCase<T>.delegate(): ReadOnlyProperty<Any?, LiveData<T>> {
    return SimpleUseCaseDelegator.from(this)
}

fun <IN, OUT> ParamSingleUseCase<IN, OUT>.delegate(params: IN): ReadOnlyProperty<Any?, LiveData<OUT>> {
    return SimpleUseCaseDelegator.from(this, params)
}

fun <IN, OUT> ParamFlowableUseCase<IN, OUT>.delegate(params: IN): ReadOnlyProperty<Any?, LiveData<OUT>> {
    return SimpleUseCaseDelegator.from(this, params)
}

fun <IN, OUT> ParamSingleUseCase<IN, OUT>.delegate(params: LiveData<IN>): ReadOnlyProperty<Any?, LiveData<OUT>> {
    return LiveUseCaseDelegator(this, params)
}

fun <IN, OUT> ParamFlowableUseCase<IN, OUT>.delegate(params: LiveData<IN>): ReadOnlyProperty<Any?, LiveData<OUT>> {
    return LiveUseCaseDelegator(this, params)
}

class SimpleUseCaseDelegator<T> private constructor() : ReadOnlyProperty<Any?, LiveData<T>> {
    private val v = MutableLiveData<T>()

    companion object {
        fun <T> from(useCase: FlowableUseCase<T>): SimpleUseCaseDelegator<T> {
            val delegate = SimpleUseCaseDelegator<T>()
            useCase.execute(onNext = { delegate.v.postValue(it) })
            return delegate
        }

        fun <In, Out> from(
            useCase: ParamSingleUseCase<In, Out>,
            param: In
        ): SimpleUseCaseDelegator<Out> {
            val delegate = SimpleUseCaseDelegator<Out>()
            useCase.execute(param, onSuccess = { delegate.v.postValue(it) })
            return delegate
        }

        fun <In, Out> from(
            useCase: ParamFlowableUseCase<In, Out>,
            param: In
        ): SimpleUseCaseDelegator<Out> {
            val delegate = SimpleUseCaseDelegator<Out>()
            useCase.execute(param, onNext = { delegate.v.postValue(it) })
            return delegate
        }
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): LiveData<T> {
        return v
    }

}

class LiveUseCaseDelegator<IN, OUT> : ReadOnlyProperty<Any?, LiveData<OUT>> {
    private var v: LiveData<OUT>

    constructor(useCase: ParamSingleUseCase<IN, OUT>, param: LiveData<IN>) {
        v = switchMap(param) {
            val live = MutableLiveData<OUT>()
            it?.let {
                useCase.execute(it, onSuccess = { result ->
                    live.postValue(result)
                })
            }
            return@switchMap live
        }
    }

    constructor(useCase: ParamFlowableUseCase<IN, OUT>, param: LiveData<IN>) {
        v = switchMap(param) {
            val live = MutableLiveData<OUT>()
            it?.let {
                useCase.execute(it, onNext = { result ->
                    live.postValue(result)
                })
            }
            return@switchMap live
        }
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>): LiveData<OUT> {
        return v
    }

}
