package com.tapac1k.spacex.common

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavArgs

open class SpaceXFragment<Event : Enum<Event>, Args : NavArgs> : Fragment() {


    fun initViewModel(
        viewModel: SpaceXViewModel<Event, Args>,
        args: Args?
    ) {

        viewModel.initialize(args)
        viewModel.events.observe(viewLifecycleOwner, Observer {
            it?.getContentIfNotConsumed()?.run {
                try {
                    onEvent(this, it.extra)
                } catch (e: NullPointerException) {
                    //No required parameter, better to have logs here
                }
            }
        })
    }

    open fun onEvent(event: Event, extras: Bundle?) {

    }
}