package com.tapac1k.spacex.di

import com.tapac1k.spacex.domain.interactor.launch.ListenLaunchesUseCase
import com.tapac1k.spacex.domain.interactor.launch.SyncLaunchesUseCase
import com.tapac1k.spacex.feature.launchlist.LaunchAdapter
import com.tapac1k.spacex.feature.launchlist.LaunchListViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val launchListModule = module {
    viewModel { LaunchListViewModel(get()) }

    factory { LaunchAdapter() }

    factory { SyncLaunchesUseCase(get(), get(), AndroidSchedulers.mainThread(), Schedulers.io()) }
    factory { ListenLaunchesUseCase(get(), AndroidSchedulers.mainThread(), Schedulers.io()) }
}