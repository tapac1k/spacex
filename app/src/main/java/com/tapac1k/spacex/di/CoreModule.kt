package com.tapac1k.spacex.di

import android.content.Context
import androidx.room.Room
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.tapac1k.data.mappers.LaunchModelToLaunchMapper
import com.tapac1k.data.mappers.LaunchResponseToLaunchMapper
import com.tapac1k.data.mappers.LaunchToLaunchEntityMapper
import com.tapac1k.data.mappers.ShortLaunchInfoToLaunchMapper
import com.tapac1k.data.repositories.ApiRepositoryImpl
import com.tapac1k.data.repositories.DatabaseRepositoryImpl
import com.tapac1k.data.repositories.DiskRepositoryImpl
import com.tapac1k.data.source.api.ApiService
import com.tapac1k.data.source.database.MainDatabase
import com.tapac1k.spacex.BuildConfig
import com.tapac1k.spacex.domain.repositories.ApiRepository
import com.tapac1k.spacex.domain.repositories.DatabaseRepository
import com.tapac1k.spacex.domain.repositories.DiskRepository
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import okhttp3.logging.HttpLoggingInterceptor.Level.NONE
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

val coreModule = module {

    factory { LaunchResponseToLaunchMapper() }
    factory { LaunchModelToLaunchMapper() }
    factory { LaunchToLaunchEntityMapper() }
    factory { ShortLaunchInfoToLaunchMapper() }

    single { get<Context>().resources }

    single<ApiRepository> {
        ApiRepositoryImpl(get(), get())
    }

    single<DatabaseRepository> {
        DatabaseRepositoryImpl(get(), get(), get(), get())
    }

    single<DiskRepository> {
        DiskRepositoryImpl(get())
    }

    single {
        Room.databaseBuilder(get(), MainDatabase::class.java, "main.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    single {
        Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter())
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    single<OkHttpClient> {
        OkHttpClient.Builder()
            .connectTimeout(60L, TimeUnit.SECONDS)
            .readTimeout(60L, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (BuildConfig.DEBUG) BODY else NONE
            })
            .build()
    }

    single<ApiService> {
        Retrofit.Builder()
            .client(get())
            .addConverterFactory(MoshiConverterFactory.create(get()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://api.spacexdata.com/")
            .build()
            .create(ApiService::class.java)
    }
}