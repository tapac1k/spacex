package com.tapac1k.spacex.di

import com.tapac1k.spacex.domain.interactor.RequestImageDownloadUseCase
import com.tapac1k.spacex.domain.interactor.launch.GetLaunchByIdUseCase
import com.tapac1k.spacex.domain.interactor.launch.GetNextLaunchIdAfterIdUseCase
import com.tapac1k.spacex.domain.interactor.launch.GetPreviousLaunchIdAfterIdUseCase
import com.tapac1k.spacex.feature.launchdetails.ImageAdapter
import com.tapac1k.spacex.feature.launchdetails.LaunchDetailsViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val launchDetailsModule = module {
    factory { GetLaunchByIdUseCase(get(), AndroidSchedulers.mainThread(), Schedulers.io()) }
    factory {
        GetNextLaunchIdAfterIdUseCase(
            get(),
            AndroidSchedulers.mainThread(),
            Schedulers.io()
        )
    }
    factory {
        GetPreviousLaunchIdAfterIdUseCase(
            get(),
            AndroidSchedulers.mainThread(),
            Schedulers.io()
        )
    }
    factory { RequestImageDownloadUseCase(get(), AndroidSchedulers.mainThread(), Schedulers.io()) }


    factory { ImageAdapter() }

    viewModel { LaunchDetailsViewModel(get(), get(), get(), get(), get()) }
}