package com.tapac1k.spacex.di

import com.tapac1k.spacex.domain.interactor.launch.GetAllLaunchesUseCase
import com.tapac1k.spacex.feature.statistic.StatisticViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val statisticsModule = module {
    factory { GetAllLaunchesUseCase(get(), AndroidSchedulers.mainThread(), Schedulers.io()) }

    viewModel { StatisticViewModel(get()) }
}