package com.tapac1k.spacex.domain.interactor.common

import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.observers.DisposableCompletableObserver

/**
 *  Base class for Completable UseCase without Parameters
 */
abstract class CompletableUseCase (
    private val mainThread: Scheduler,
    private val ioThread: Scheduler
) {

    internal abstract fun buildUseCaseObservable(): Completable

    fun execute(
        onComplete: (() -> Unit)? = null,
        onError: ((Throwable) -> Unit)? = null
    ): DisposableCompletableObserver {

        return this.buildUseCaseObservable()
            .subscribeOn(ioThread)
            .observeOn(mainThread)
            .subscribeWith(object : DisposableCompletableObserver() {
                override fun onComplete() {
                    onComplete?.invoke()
                }

                override fun onError(e: Throwable) {
                    onError?.invoke(e)
                }
            })
    }


}