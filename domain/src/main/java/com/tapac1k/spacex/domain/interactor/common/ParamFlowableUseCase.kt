package com.tapac1k.spacex.domain.interactor.common

import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.subscribers.ResourceSubscriber

/**
 *  Base class for Flowable UseCase With Parameters
 *
 *  @param Input specifies input
 *  @param Output specifies items in flowable stream
 */
abstract class ParamFlowableUseCase<Input, Output>(
    private val mainThread: Scheduler,
    private val ioThread: Scheduler
) {

    internal abstract fun buildUseCaseObservable(input: Input): Flowable<Output>

    fun execute(
        input: Input,
        onNext: ((Output) -> Unit)? = null,
        onError: ((Throwable) -> Unit)? = null
    ): ResourceSubscriber<Output> {
        return this.buildUseCaseObservable(input)
            .subscribeOn(ioThread)
            .observeOn(mainThread)
            .subscribeWith(object : ResourceSubscriber<Output>() {
                override fun onComplete() {
                }

                override fun onNext(t: Output) {
                    onNext?.invoke(t)
                }

                override fun onError(e: Throwable) {
                    onError?.invoke(e)
                }
            })
    }


}