package com.tapac1k.spacex.domain.repositories

import com.tapac1k.spacex.domain.entities.Launch
import io.reactivex.Single

interface ApiRepository {
    /**
     * Get list of launches from API. Default domain: https://api.spacexdata.com/v3/launches/
     * [limit] - max item count in result list
     * [offset] - skip results from the beginning of the query
     */
    fun getLaunches(limit: Int, offset: Int): Single<List<Launch>>
}