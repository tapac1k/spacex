package com.tapac1k.spacex.domain.interactor

import com.tapac1k.spacex.domain.interactor.common.ParamCompletableUseCase
import com.tapac1k.spacex.domain.repositories.DiskRepository
import io.reactivex.Completable
import io.reactivex.Scheduler

/**
 * Interactor for image downloading via download manager. More info: [DiskRepository.requestDownloadImage]
 *
 * @property diskRepository - implement connection to downloadManager
 */
class RequestImageDownloadUseCase(
    private val diskRepository: DiskRepository,
    mainThread: Scheduler,
    ioThread: Scheduler
) : ParamCompletableUseCase<RequestImageDownloadUseCase.Params>(mainThread, ioThread) {

    override fun buildUseCaseObservable(input: Params): Completable {
        return diskRepository.requestDownloadImage(
            input.remoteUrl,
            input.filename,
            input.downloadTitle
        )
    }

    class Params(
        val remoteUrl: String,
        val filename: String,
        val downloadTitle: String
    )
}