package com.tapac1k.spacex.domain.interactor.common

import io.reactivex.Flowable
import io.reactivex.Scheduler
import io.reactivex.subscribers.ResourceSubscriber

/**
 *  Base class for Flowable UseCase Without Parameters
 *
 *  @param Output specifies items in flowable stream
 */
abstract class FlowableUseCase<Output>(
    private val mainThread: Scheduler,
    private val ioThread: Scheduler
) {

    internal abstract fun buildUseCaseObservable(): Flowable<Output>

    fun execute(
        onNext: ((Output) -> Unit)? = null,
        onError: ((Throwable) -> Unit)? = null
    ): ResourceSubscriber<Output> {
        return this.buildUseCaseObservable()
            .subscribeOn(ioThread)
            .observeOn(mainThread)
            .subscribeWith(object : ResourceSubscriber<Output>() {
                override fun onComplete() {
                }

                override fun onNext(t: Output) {
                    onNext?.invoke(t)
                }

                override fun onError(e: Throwable) {
                    onError?.invoke(e)
                }
            })
    }


}