package com.tapac1k.spacex.domain.interactor.launch

import com.tapac1k.spacex.domain.interactor.common.CompletableUseCase
import com.tapac1k.spacex.domain.repositories.ApiRepository
import com.tapac1k.spacex.domain.repositories.DatabaseRepository
import io.reactivex.Completable
import io.reactivex.Scheduler

private const val PAGE_LIMIT = 30

class SyncLaunchesUseCase(
    private val apiRepository: ApiRepository,
    private val databaseRepository: DatabaseRepository,
    mainThread: Scheduler,
    ioThread: Scheduler
) : CompletableUseCase(mainThread, ioThread) {

    override fun buildUseCaseObservable(): Completable {
        return Completable.fromAction {
            var offset = 0
            do {
                val list = apiRepository.getLaunches(PAGE_LIMIT, offset).blockingGet()
                databaseRepository.writeLaunches(list).blockingAwait()
                offset += PAGE_LIMIT
            } while (list.isNotEmpty())
        }
    }
}