package com.tapac1k.spacex.domain.interactor.common

import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver

/**
 *  Base class for Single UseCase With Parameters
 *
 *  @param Input specifies input
 *  @param Output specifies items in Single stream
 */
abstract class ParamSingleUseCase<Input, Output>(
    private val mainThread: Scheduler,
    private val ioThread: Scheduler
) {

    internal abstract fun buildUseCaseObservable(input: Input): Single<Output>

    fun execute(
        input: Input,
        onSuccess: ((Output) -> Unit)? = null,
        onError: ((Throwable) -> Unit)? = null
    ): DisposableSingleObserver<Output> {
        return this.buildUseCaseObservable(input)
            .subscribeOn(ioThread)
            .observeOn(mainThread)
            .subscribeWith(object : DisposableSingleObserver<Output>() {
                override fun onSuccess(t: Output) {
                    onSuccess?.invoke(t)
                }

                override fun onError(e: Throwable) {
                    onError?.invoke(e)
                }
            })
    }


}