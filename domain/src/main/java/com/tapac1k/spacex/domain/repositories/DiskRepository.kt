package com.tapac1k.spacex.domain.repositories

import io.reactivex.Completable

interface DiskRepository {

    /**
     * Request donwload manager to download image
     * Completable stream completes when donwload manager starts downloading
     *
     * Image will be saved in public download directory
     *
     * @param remoteUrl - link to download
     * @param filename - filename should contain file extension
     * @param downloadTitle - title for DownloadManager notification
     */
    fun requestDownloadImage(
        remoteUrl: String,
        filename: String,
        downloadTitle: String
    ): Completable
}