package com.tapac1k.spacex.domain.interactor.common

import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.observers.DisposableCompletableObserver

/**
 *  Base class for Completable UseCase with Parameters
 *
 *  @param Input specifies input
 */
abstract class ParamCompletableUseCase<Input> (
    private val mainThread: Scheduler,
    private val ioThread: Scheduler
) {

    internal abstract fun buildUseCaseObservable(input: Input): Completable

    fun execute(
        input: Input,
        onComplete: (() -> Unit)? = null,
        onError: ((Throwable) -> Unit)? = null
    ): DisposableCompletableObserver {

        return this.buildUseCaseObservable(input)
            .subscribeOn(ioThread)
            .observeOn(mainThread)
            .subscribeWith(object : DisposableCompletableObserver() {
                override fun onComplete() {
                    onComplete?.invoke()
                }

                override fun onError(e: Throwable) {
                    onError?.invoke(e)
                }
            })
    }


}