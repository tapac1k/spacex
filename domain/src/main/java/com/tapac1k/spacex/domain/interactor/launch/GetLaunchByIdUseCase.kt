package com.tapac1k.spacex.domain.interactor.launch

import com.tapac1k.spacex.domain.entities.Launch
import com.tapac1k.spacex.domain.interactor.common.ParamSingleUseCase
import com.tapac1k.spacex.domain.repositories.DatabaseRepository
import io.reactivex.Scheduler
import io.reactivex.Single

/**
 * Interactor to get launch by its id
 *
 * @property databaseRepository used for database connection
 */
class GetLaunchByIdUseCase(
    private val databaseRepository: DatabaseRepository,
    mainThread: Scheduler,
    ioThread: Scheduler
): ParamSingleUseCase<Int, Launch>(mainThread, ioThread) {

    /**
     * [input] - launch id, same as [Launch.flightNumber]
     */
    override fun buildUseCaseObservable(input: Int): Single<Launch> {
        return databaseRepository.getLaunchById(input)
    }
}