package com.tapac1k.spacex.domain.interactor.launch

import com.tapac1k.spacex.domain.entities.Launch
import com.tapac1k.spacex.domain.interactor.common.FlowableUseCase
import com.tapac1k.spacex.domain.repositories.DatabaseRepository
import io.reactivex.Flowable
import io.reactivex.Scheduler

/**
 * Interactor to subscribe to launch table update
 *
 * @property databaseRepository used for database connection
 */
class ListenLaunchesUseCase(
    private val databaseRepository: DatabaseRepository,
    mainThread: Scheduler,
    ioThread: Scheduler
) : FlowableUseCase<List<Launch>>(mainThread, ioThread) {

    override fun buildUseCaseObservable(): Flowable<List<Launch>> {
        return databaseRepository.listenLaunches()
    }


}