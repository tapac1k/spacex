package com.tapac1k.spacex.domain.entities

import java.util.*

data class Launch(
    val flightNumber: Int,
    val missionName: String,
    val launchDate: Date,

    val rocketName: String? = null,
    val success: Boolean?,
    val upcoming: Boolean,
    val details: String? = null,

    val images: List<String> = emptyList(),
    val badgeLink: String? = null,
    val badgeLinkLarge: String? = null,
    val youtubeLink: String? = null,
    val wikipediaLink: String? = null,
    val pressKitLink: String? = null,
    val moreImagesLink: String? = null
)

