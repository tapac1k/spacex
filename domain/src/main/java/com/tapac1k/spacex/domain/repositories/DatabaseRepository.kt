package com.tapac1k.spacex.domain.repositories

import com.tapac1k.spacex.domain.entities.Launch
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface DatabaseRepository {
    /**
     * Subscribe to launch table update.
     */
    fun listenLaunches(): Flowable<List<Launch>>

    /**
     * Insert [launches] to database
     */
    fun writeLaunches(launches: List<Launch>): Completable

    /**
     * Get launch from database by [id]
     * [id] is same as [Launch.flightNumber]
     */
    fun getLaunchById(id: Int): Single<Launch>

    /**
     * Get id of next launch from database after [id]
     * [id] is same as [Launch.flightNumber]
     */
    fun getNextLaunchIdAfterId(id: Int): Single<Int>

    /**
     * Get id of previous launch from database after [id]
     * [id] is same as [Launch.flightNumber]
     */
    fun getPreviousLaunchIdAfterId(id: Int): Single<Int>


    /**
     * Subscribe to all launches in local database
     *
     * Launches have only short info:
     * [Launch.flightNumber]
     * [Launch.missionName]
     * [Launch.success]
     * [Launch.upcoming]
     * [Launch.launchDate]
     *
     * Other fields are null or empty
     */
    fun getAllLaunchesShortInfo(): Flowable<List<Launch>>
}