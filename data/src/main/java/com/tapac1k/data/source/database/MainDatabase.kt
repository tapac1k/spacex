package com.tapac1k.data.source.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.tapac1k.data.source.database.image.ImageDao
import com.tapac1k.data.source.database.image.ImageEntity
import com.tapac1k.data.source.database.launch.LaunchDao
import com.tapac1k.data.source.database.launch.LaunchEntity

@Database(
    entities = [
        LaunchEntity::class,
        ImageEntity::class
    ],
    version = 1
)
@TypeConverters(Converters::class)
abstract class MainDatabase : RoomDatabase() {
    abstract fun launchDao(): LaunchDao
    abstract fun imageDao(): ImageDao
}