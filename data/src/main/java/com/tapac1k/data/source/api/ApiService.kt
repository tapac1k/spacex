package com.tapac1k.data.source.api

import com.tapac1k.data.source.api.entities.LaunchResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/v3/launches")
    fun getLaunches(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Single<List<LaunchResponse>>
}