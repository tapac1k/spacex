package com.tapac1k.data.source.database.launch

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "launch")
class LaunchEntity(
    @PrimaryKey
    @ColumnInfo(name = "flight_number") val flightNumber: Int,
    @ColumnInfo(name = "mission_name") val missionName: String,
    @ColumnInfo(name = "launch_date") val launchDate: Date,
    @ColumnInfo(name = "rocket_name") val rocketName: String?,
    @ColumnInfo(name = "success") val success: Boolean?,
    @ColumnInfo(name = "upcoming") val upcoming: Boolean,
    @ColumnInfo(name = "details") val details: String?,
    @ColumnInfo(name = "bage_link") val bageLink: String?,
    @ColumnInfo(name = "bage_link_large") val bageLinkLarge: String?,
    @ColumnInfo(name = "youtube_link") val youtubeLink: String?,
    @ColumnInfo(name = "wikipedia_link") val wikipediaLink: String?,
    @ColumnInfo(name = "press_kit_link") val pressKitLink: String?,
    @ColumnInfo(name = "more_images_link") val moreImagesLink: String?
)