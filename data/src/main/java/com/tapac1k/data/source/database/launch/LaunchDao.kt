package com.tapac1k.data.source.database.launch

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.tapac1k.data.source.database.BaseDao
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
abstract class LaunchDao: BaseDao<LaunchEntity>() {

    @Transaction
    @Query("SELECT * FROM launch ORDER BY flight_number")
    abstract fun listenLaunches(): Flowable<List<LaunchModel>>

    @Transaction
    @Query("SELECT * FROM launch WHERE flight_number = :id")
    abstract fun getLaunchById(id: Int): Single<LaunchModel>

    @Transaction
    @Query("SELECT flight_number FROM launch WHERE flight_number > :id ORDER BY flight_number LIMIT 1 ")
    abstract fun getNextLaunchIdById(id: Int): Single<Int>

    @Transaction
    @Query("SELECT flight_number FROM launch WHERE flight_number < :id ORDER BY flight_number DESC LIMIT 1")
    abstract fun getPreviousLaunchIdById(id: Int): Single<Int>

    @Query("SELECT flight_number, mission_name, launch_date, success, upcoming FROM launch")
    abstract fun listenAllLaunchesShortInfo(): Flowable<List<ShortLaunchInfo>>
}