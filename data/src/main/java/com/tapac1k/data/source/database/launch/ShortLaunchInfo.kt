package com.tapac1k.data.source.database.launch

import androidx.room.ColumnInfo
import java.util.*

class ShortLaunchInfo(
    @ColumnInfo(name = "flight_number") val flightNumber: Int,
    @ColumnInfo(name = "mission_name") val missionName: String,
    @ColumnInfo(name = "launch_date") val launchDate: Date,
    @ColumnInfo(name = "success") val success: Boolean?,
    @ColumnInfo(name = "upcoming") val upcoming: Boolean )