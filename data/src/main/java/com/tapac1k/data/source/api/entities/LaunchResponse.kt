package com.tapac1k.data.source.api.entities

import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
class LaunchResponse(
    val flight_number: Int,
    val mission_name: String,
    val launch_date_utc: Date,
    val launch_success: Boolean?,
    val upcoming: Boolean,
    val links: LinkResponse?,
    val details: String?,
    val rocket: RocketResponse?
)



