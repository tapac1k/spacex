package com.tapac1k.data.source.database.launch

import androidx.room.Embedded
import androidx.room.Relation
import com.tapac1k.data.source.database.image.ImageEntity

class LaunchModel (
    @Embedded
    val launchEntity: LaunchEntity
) {

    @Relation(entity = ImageEntity::class , parentColumn = "flight_number", entityColumn = "launch_id")
    var images: List<ImageEntity>? = null
}