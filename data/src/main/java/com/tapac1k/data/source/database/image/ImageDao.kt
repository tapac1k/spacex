package com.tapac1k.data.source.database.image

import androidx.room.Dao
import com.tapac1k.data.source.database.BaseDao

@Dao
abstract class ImageDao: BaseDao<ImageEntity>()