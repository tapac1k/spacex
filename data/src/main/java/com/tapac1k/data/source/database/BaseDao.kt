package com.tapac1k.data.source.database

import androidx.room.*

private const val IGNORED = -1L

abstract class BaseDao<in T> {


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(value: T): Long

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun insert(values: List<T>): List<Long>

    @Update
    abstract fun update(value: T)

    @Update
    abstract fun update(values: List<T>)

    @Delete
    abstract fun delete(value: T)

    open fun insertOrUpdate(value: T) {
        val id = insert(value)
        if (id == IGNORED) {
            update(value)
        }
    }

    @Transaction
    open fun insertOrUpdate(list: List<T>) {
        list.forEach {
            insertOrUpdate(it)
        }
    }
}