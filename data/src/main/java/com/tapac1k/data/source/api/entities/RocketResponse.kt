package com.tapac1k.data.source.api.entities

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class RocketResponse(val rocket_name: String)