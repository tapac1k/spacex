package com.tapac1k.data.source.api.entities

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class LinkResponse(
    val mission_patch_small: String?,
    val mission_patch: String?,
    val reddit_media: String?,
    val presskit: String?,
    val video_link: String?,
    val wikipedia: String?,
    val flickr_images: List<String>?
)