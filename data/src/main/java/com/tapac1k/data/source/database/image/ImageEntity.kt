package com.tapac1k.data.source.database.image

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "launch_image",
   primaryKeys = ["launch_id","index"])
class ImageEntity(
   @ColumnInfo(name = "launch_id") val launchId: Int,
   @ColumnInfo(name = "index") val index: Int,
   @ColumnInfo(name = "url") val url: String
)