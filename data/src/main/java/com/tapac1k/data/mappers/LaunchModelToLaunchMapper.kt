package com.tapac1k.data.mappers

import com.tapac1k.data.source.database.launch.LaunchModel
import com.tapac1k.spacex.domain.entities.Launch

class LaunchModelToLaunchMapper {
    fun map(launchModel: LaunchModel): Launch {
        return Launch(
            flightNumber = launchModel.launchEntity.flightNumber,
            missionName = launchModel.launchEntity.missionName,
            launchDate = launchModel.launchEntity.launchDate,
            rocketName = launchModel.launchEntity.rocketName,
            success = launchModel.launchEntity.success,
            upcoming = launchModel.launchEntity.upcoming,
            details = launchModel.launchEntity.details,
            badgeLink = launchModel.launchEntity.bageLink,
            youtubeLink = launchModel.launchEntity.youtubeLink,
            wikipediaLink = launchModel.launchEntity.wikipediaLink,
            pressKitLink = launchModel.launchEntity.pressKitLink,
            moreImagesLink = launchModel.launchEntity.moreImagesLink,
            images = launchModel.images?.map { it.url }?: emptyList(),
            badgeLinkLarge = launchModel.launchEntity.bageLinkLarge
        )
    }

    fun map(launchModels: List<LaunchModel>): List<Launch> {
        return launchModels.map(::map)
    }
}