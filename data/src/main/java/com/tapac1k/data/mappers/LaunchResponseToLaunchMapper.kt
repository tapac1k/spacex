package com.tapac1k.data.mappers

import com.tapac1k.data.source.api.entities.LaunchResponse
import com.tapac1k.spacex.domain.entities.Launch

class LaunchResponseToLaunchMapper {

    fun map(response: LaunchResponse): Launch {
        return with(response) {
            Launch(
                flightNumber = flight_number,
                missionName = mission_name,
                launchDate = launch_date_utc,
                rocketName = rocket?.rocket_name,
                success = launch_success,
                upcoming = upcoming,
                details = details,
                images = links?.flickr_images ?: emptyList(),
                badgeLink = links?.mission_patch_small,
                youtubeLink = links?.video_link,
                wikipediaLink = links?.wikipedia,
                pressKitLink = links?.presskit,
                moreImagesLink = links?.reddit_media,
                badgeLinkLarge = links?.mission_patch
            )
        }
    }

    fun map(responses: List<LaunchResponse>): List<Launch> {
        return responses.map(::map)
    }
}