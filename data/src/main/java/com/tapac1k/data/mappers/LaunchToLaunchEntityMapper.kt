package com.tapac1k.data.mappers

import com.tapac1k.data.source.database.launch.LaunchEntity
import com.tapac1k.spacex.domain.entities.Launch

class LaunchToLaunchEntityMapper {

    fun map(launch: Launch): LaunchEntity {
        return LaunchEntity(
            flightNumber = launch.flightNumber,
            missionName = launch.missionName,
            launchDate = launch.launchDate,
            rocketName = launch.rocketName,
            success = launch.success,
            upcoming = launch.upcoming,
            details = launch.details,
            bageLink = launch.badgeLink,
            youtubeLink = launch.youtubeLink,
            wikipediaLink = launch.wikipediaLink,
            pressKitLink = launch.pressKitLink,
            moreImagesLink = launch.moreImagesLink,
            bageLinkLarge = launch.badgeLinkLarge
        )
    }
}