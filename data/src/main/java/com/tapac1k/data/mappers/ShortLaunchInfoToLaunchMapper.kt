package com.tapac1k.data.mappers

import com.tapac1k.data.source.database.launch.ShortLaunchInfo
import com.tapac1k.spacex.domain.entities.Launch

class ShortLaunchInfoToLaunchMapper {

    fun map(shortInfo: ShortLaunchInfo): Launch {
        return Launch(
            flightNumber = shortInfo.flightNumber,
            missionName = shortInfo.missionName,
            launchDate = shortInfo.launchDate,
            success = shortInfo.success,
            upcoming = shortInfo.upcoming
        )
    }

    fun map(list: List<ShortLaunchInfo>): List<Launch> {
        return list.map(::map)
    }
}