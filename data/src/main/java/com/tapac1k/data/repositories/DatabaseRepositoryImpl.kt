package com.tapac1k.data.repositories

import com.tapac1k.data.mappers.LaunchModelToLaunchMapper
import com.tapac1k.data.mappers.LaunchToLaunchEntityMapper
import com.tapac1k.data.mappers.ShortLaunchInfoToLaunchMapper
import com.tapac1k.data.source.database.MainDatabase
import com.tapac1k.data.source.database.image.ImageEntity
import com.tapac1k.spacex.domain.entities.Launch
import com.tapac1k.spacex.domain.repositories.DatabaseRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class DatabaseRepositoryImpl(
    mainDatabase: MainDatabase,
    private val launchEntityMapper: LaunchToLaunchEntityMapper,
    private val launchModelMapper: LaunchModelToLaunchMapper,
    private val shortLaunchInfoToLaunchMapper: ShortLaunchInfoToLaunchMapper
): DatabaseRepository {
    private val launchDao = mainDatabase.launchDao()
    private val imageDao = mainDatabase.imageDao()

    override fun listenLaunches(): Flowable<List<Launch>> {
        return launchDao.listenLaunches().map(launchModelMapper::map)
    }

    override fun writeLaunches(launches: List<Launch>): Completable {
        return Completable.fromAction{
            launchDao.insertOrUpdate(launches.map(launchEntityMapper::map))
            val images = launches.map { launch ->
                launch.images.mapIndexed { i, value ->
                    ImageEntity(launch.flightNumber, i, value)
                }
            }.flatten()
            imageDao.insert(images)
        }
    }

    override fun getLaunchById(id: Int): Single<Launch> {
        return launchDao.getLaunchById(id).map(launchModelMapper::map)
    }

    override fun getNextLaunchIdAfterId(id: Int): Single<Int> {
        return launchDao.getNextLaunchIdById(id)
    }

    override fun getPreviousLaunchIdAfterId(id: Int): Single<Int> {
        return launchDao.getPreviousLaunchIdById(id)
    }

    override fun getAllLaunchesShortInfo(): Flowable<List<Launch>> {
        return launchDao.listenAllLaunchesShortInfo().map(shortLaunchInfoToLaunchMapper::map)
    }
}