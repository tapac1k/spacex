package com.tapac1k.data.repositories

import com.tapac1k.data.mappers.LaunchResponseToLaunchMapper
import com.tapac1k.data.source.api.ApiService
import com.tapac1k.spacex.domain.entities.Launch
import com.tapac1k.spacex.domain.repositories.ApiRepository
import io.reactivex.Single

class ApiRepositoryImpl(private val apiService: ApiService,
                        private val mapper: LaunchResponseToLaunchMapper): ApiRepository {

    override fun getLaunches(limit: Int, offset: Int): Single<List<Launch>> {
        return apiService.getLaunches(limit,offset).map(mapper::map)
    }
}