package com.tapac1k.data.repositories

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import com.tapac1k.spacex.domain.repositories.DiskRepository
import io.reactivex.Completable

class DiskRepositoryImpl(val context: Context) : DiskRepository {
    override fun requestDownloadImage(
        remoteUrl: String,
        filename: String,
        downloadTitle: String
    ): Completable {

        return Completable.fromAction {
            val downloadManager =
                context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

            val request = DownloadManager.Request(Uri.parse(remoteUrl))
                .setTitle(downloadTitle)
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, filename)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            downloadManager.enqueue(request)

        }
    }
}